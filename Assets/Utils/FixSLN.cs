using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

public class FixSLN : MonoBehaviour
{
    [UnityEditor.MenuItem("Assets/Regen Sln", false, 1000)]
    public static void RegenerateSolution()
    {
        var sync_vs_type = Type.GetType("UnityEditor.SyncVS,UnityEditor");

        var synchronizer_field = sync_vs_type.GetField("Synchronizer", BindingFlags.NonPublic | BindingFlags.Static);
        var sync_solution_mi = sync_vs_type.GetMethod("SyncSolution", BindingFlags.Public | BindingFlags.Static);

        var synchronizer_object = synchronizer_field.GetValue(sync_vs_type);
        var synchronizer_type = synchronizer_object.GetType();
        var synchronizer_sync_mi = synchronizer_type.GetMethod("Sync", BindingFlags.Public | BindingFlags.Instance);

        sync_solution_mi.Invoke(null, null);
        synchronizer_sync_mi.Invoke(synchronizer_object, null);
    }

}
