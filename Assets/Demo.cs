using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Demo : MonoBehaviour
{
    private Queue<string> messages;
    private RealmClientSample realm;
    [SerializeField] private RealmClientConfiguration realmClientConfiguration;
    
    private void Start()
    {
        if (realmClientConfiguration == null || !realmClientConfiguration.IsValid())
        {
            Debug.LogError("RealmClientConfiguration is null or invalid");
            enabled = false;
            return;
        }

        messages = new Queue<string>();
        realm = new RealmClientSample(messages);
        Task.Run(()=>realm.Init(realmClientConfiguration));
    }

    private void Update()
    {
        while(messages.Count>0)
            Debug.Log(messages.Dequeue());
    }
    
    public void OnDestroy()
    {
        realm?.Dispose();
        realm = null;
    }
    
}
