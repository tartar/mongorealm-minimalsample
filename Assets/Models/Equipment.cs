using System;
using MongoDB.Bson;
using Realms;

[Serializable]
public class Equipment : RealmObject
{
    public ObjectId _id { get; set; }
    public string Name { get; set; }
    public int GoldValue { get; set; }
}
