using System;
using MongoDB.Bson;

[Serializable]
public class Inventory
{
    public int Gold { get; set; }
    public ObjectId[] Equipments { get; set; }
}
