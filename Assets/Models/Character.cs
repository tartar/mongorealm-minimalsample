using System;
using MongoDB.Bson;
using Realms;

[Serializable]
public class Character : RealmObject
{
    [PrimaryKey]
    public ObjectId _id { get; set; }
    public string Name { get; set; }
    public Inventory Inventory { get; set; }
    public int Defense { get; set; }
}
