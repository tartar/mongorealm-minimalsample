using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using Realms;
using Realms.Sync;

public class RealmClientSample : IDisposable
{
    private readonly Queue<string> debugMessages;
    
    public RealmClientSample(Queue<string> debugMessages)
    {
        this.debugMessages = debugMessages;
    }

    public async Task Init(RealmClientConfiguration configuration)
    {
        debugMessages.Enqueue("Start Realm Initialization");
        var myRealmAppId = configuration.RealmAppId;


        debugMessages.Enqueue("Create Realm App instance");
        var app = App.Create(myRealmAppId);

        debugMessages.Enqueue("Login with user credentials");
        var credentials = Credentials.EmailPassword(configuration.UserEmail, configuration.UserPassword);
        //Crash
        //var user = await app.LogInAsync(credentials);

        debugMessages.Enqueue("Get Realm instance");
        var realm = await Realm.GetInstanceAsync();
        
        debugMessages.Enqueue("Load data");
        var characters = realm.All<Character>();
        
        debugMessages.Enqueue($"Found {characters.Count()} characters");
    }

    public void Dispose()
    {
        Realm.GetInstance().Dispose();
    }
}
