using UnityEngine;

[CreateAssetMenu(fileName = "RealmClientConfiguration", menuName = "RealmClient/RealmClientConfiguration")]
public class RealmClientConfiguration : ScriptableObject
{
    public string RealmAppId;
    public string UserEmail;
    public string UserPassword;

    public bool IsValid()
    {
        if (string.IsNullOrEmpty(RealmAppId))
        {
            Debug.LogError("RealmAppId is null or empty in RealmClientConfiguration");
            return false;
        }

        if (string.IsNullOrEmpty(UserEmail) || !IsValidEmail(UserEmail))
        {
            Debug.LogError("UserEmail is null, empty or has invalid format in RealmClientConfiguration");
            return false;
        }

        if (string.IsNullOrEmpty(UserPassword))
        {
            Debug.LogError("UserPassword is null or empty in RealmClientConfiguration");
            return false;
        }

        return true;
    }

    bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }
}
